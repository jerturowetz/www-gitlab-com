---
layout: handbook-page-toc
title: "Manager Toolkit"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This page will consist of guides and tools to help managers in their roles.

#### [Compensation Review](/handbook/people-group/manager-toolkit/compensation-review)