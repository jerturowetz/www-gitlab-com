---
layout: handbook-page-toc
title: "CS Top 10 List for Product"
---

# CS Top 10 List for Product
{:.no_toc}

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

- [Technical Account Manager Summary](/handbook/customer-success/tam/)
- [CS Top 10](/handbook/customer-success/tam/cs-top-10/) *(Current)*
- [Capturing Customer Interest in GitLab Issues](/handbook/customer-success/tam/customer-issue-interest/)
- [Executive Business Reviews (EBRs)](/handbook/customer-success/tam/ebr/)
- [Account Engagement](/handbook/customer-success/tam/engagement/)
- [Escalation Process](/handbook/customer-success/tam/escalations/)
- [Gemstones](/handbook/customer-success/tam/gemstones/)
- [Customer Health Scores](/handbook/customer-success/tam/health-scores/)
- [Account Onboarding](/handbook/customer-success/tam/onboarding/)
- [Customer Renewal Tracking](/handbook/customer-success/tam/renewals/)
- [Account Triage](/handbook/customer-success/tam/triage/)

### Related Pages

- [Using Salesforce within Customer Success](/handbook/customer-success/using-salesforce-within-customer-success/)
- [Responsibility Matrix and Transitions](/handbook/customer-success/#responsibility-matrix-and-transitions/)
- [Customer Success & Market Segmentation](/handbook/customer-success/#customer-success--market-segmentation/)
- [Customer Success Vision](/handbook/customer-success/vision/)
- [Support handbook](/handbook/support/)
- [Sales handbook](/handbook/sales/)

---

## What
One of the [product sensing mechanisms](/handbook/product/#sensing-mechanisms) is to collect feedback from Customer Success on the top issues customers are looking for in the product.  To aid in gathering this information, the TAM team meets quarterly to collect and filter this information to the Product team using the process below.

## Process

1. TAMs gather customers' most requested and urgent product requests
1. Quarterly, the TAM team will meet to go over the most requested features and gather all those issues into a document
1. Together, the TAM team comes up with the most popular and most needed 20-30 features
1. Through some voting mechism, the entire Customer Success team votes on the 20-30 features to make this quarter's "Top 10 List" (can be up to 12 issues with ties)
1. TAMs will give results of voting to Product by opening an issue on the [product issue board](https://gitlab.com/gitlab-com/Product/issues/new?issuable_template=CS-Top-10) and attaching them there.
1. TAM representative will attend the next Weekly Product meeting to present the results.
1. After that, Product will take over and follow the process noted in the [issue template](https://gitlab.com/gitlab-com/Product/blob/master/.gitlab/issue_templates/CS-Top-10.md)
